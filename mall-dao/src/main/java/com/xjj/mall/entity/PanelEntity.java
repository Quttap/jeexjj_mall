/****************************************************
 * Description: Entity for 内容分类
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.util.Date;
import java.util.List;

import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class PanelEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public PanelEntity(){}
    private String name;//板块名称
    private Integer type;//类型 0轮播图 1板块种类一 2板块种类二 3板块种类三 
    private Integer sortOrder;//排列序号
    private Integer position;//所属位置 0首页 1商品推荐 2我要捐赠
    private Integer limitNum;//板块限制商品数量
    private Integer status;//状态
    private String remark;//备注
    private Date created;//创建时间
    private Date updated;//更新时间
    
    private List<PanelContentEntity> panelContents;
    /**
     * 返回板块名称
     * @return 板块名称
     */
    public String getName() {
        return name;
    }
    
    /**
     * 设置板块名称
     * @param name 板块名称
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * 返回类型 0轮播图 1板块种类一 2板块种类二 3板块种类三 
     * @return 类型 0轮播图 1板块种类一 2板块种类二 3板块种类三 
     */
    public Integer getType() {
        return type;
    }
    
    /**
     * 设置类型 0轮播图 1板块种类一 2板块种类二 3板块种类三 
     * @param type 类型 0轮播图 1板块种类一 2板块种类二 3板块种类三 
     */
    public void setType(Integer type) {
        this.type = type;
    }
    
    /**
     * 返回排列序号
     * @return 排列序号
     */
    public Integer getSortOrder() {
        return sortOrder;
    }
    
    /**
     * 设置排列序号
     * @param sortOrder 排列序号
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    /**
     * 返回所属位置 0首页 1商品推荐 2我要捐赠
     * @return 所属位置 0首页 1商品推荐 2我要捐赠
     */
    public Integer getPosition() {
        return position;
    }
    
    /**
     * 设置所属位置 0首页 1商品推荐 2我要捐赠
     * @param position 所属位置 0首页 1商品推荐 2我要捐赠
     */
    public void setPosition(Integer position) {
        this.position = position;
    }
    
    /**
     * 返回板块限制商品数量
     * @return 板块限制商品数量
     */
    public Integer getLimitNum() {
        return limitNum;
    }
    
    /**
     * 设置板块限制商品数量
     * @param limitNum 板块限制商品数量
     */
    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }
    
    /**
     * 返回状态
     * @return 状态
     */
    public Integer getStatus() {
        return status;
    }
    
    /**
     * 设置状态
     * @param status 状态
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    
    /**
     * 返回备注
     * @return 备注
     */
    public String getRemark() {
        return remark;
    }
    
    /**
     * 设置备注
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
    
    /**
     * 返回创建时间
     * @return 创建时间
     */
    public Date getCreated() {
        return created;
    }
    
    /**
     * 设置创建时间
     * @param created 创建时间
     */
    public void setCreated(Date created) {
        this.created = created;
    }
    
    /**
     * 返回更新时间
     * @return 更新时间
     */
    public Date getUpdated() {
        return updated;
    }
    
    /**
     * 设置更新时间
     * @param updated 更新时间
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public List<PanelContentEntity> getPanelContents() {
		return panelContents;
	}

	public void setPanelContents(List<PanelContentEntity> panelContents) {
		this.panelContents = panelContents;
	}

	public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.PanelEntity").append("ID="+this.getId()).toString();
    }
}

