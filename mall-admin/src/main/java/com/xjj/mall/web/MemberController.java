/****************************************************
 * Description: Controller for 用户表
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
	*  2018-09-13 zhanghejie Create File
**************************************************/
package com.xjj.mall.web;
import com.xjj.mall.entity.MemberEntity;
import com.xjj.mall.service.MemberService;
import java.util.Date;
import com.xjj.framework.json.XjjJson;
import com.xjj.framework.exception.ValidationException;
import com.xjj.framework.web.SpringControllerSupport;
import com.xjj.framework.web.support.Pagination;
import com.xjj.framework.web.support.QueryParameter;
import com.xjj.framework.web.support.XJJParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xjj.framework.security.annotations.SecCreate;
import com.xjj.framework.security.annotations.SecDelete;
import com.xjj.framework.security.annotations.SecEdit;
import com.xjj.framework.security.annotations.SecList;
import com.xjj.framework.security.annotations.SecPrivilege;

@Controller
@RequestMapping("/mall/member")
public class MemberController extends SpringControllerSupport{
	@Autowired
	private MemberService memberService;
	
	
	@SecPrivilege(title="用户管理")
	@RequestMapping(value = "/index")
	public String index(Model model) {
		String page = this.getViewPath("index");
		return page;
	}
	
	@SecList
	@RequestMapping(value = "/list")
	public String list(Model model,
			@QueryParameter XJJParameter query,
			@ModelAttribute("page") Pagination page
			) {
		page = memberService.findPage(query,page);
		return getViewPath("list");
	}
	
	@SecCreate
	@RequestMapping("/input")
	public String create(@ModelAttribute("member") MemberEntity member,Model model){
		return getViewPath("input");
	}
	
	@SecEdit
	@RequestMapping("/input/{id}")
	public String edit(@PathVariable("id") Long id, Model model){
		MemberEntity member = memberService.getById(id);
		model.addAttribute("member",member);
		return getViewPath("input");
	}
	
	@SecCreate
	@SecEdit
	@RequestMapping("/save")
	public @ResponseBody XjjJson save(@ModelAttribute MemberEntity member){
		
		validateSave(member);
		if(member.isNew())
		{
			//member.setCreateDate(new Date());
			memberService.save(member);
		}else
		{
			memberService.update(member);
		}
		return XjjJson.success("保存成功");
	}
	
	
	/**
	 * 数据校验
	 **/
	private void validateSave(MemberEntity member){
		//必填项校验
		// 判断用户名是否为空
		if(null==member.getUsername()){
			throw new ValidationException("校验失败，用户名不能为空！");
		}
		// 判断密码，加密存储是否为空
		if(null==member.getPassword()){
			throw new ValidationException("校验失败，密码，加密存储不能为空！");
		}
		// 判断created是否为空
		if(null==member.getCreated()){
			throw new ValidationException("校验失败，created不能为空！");
		}
		// 判断updated是否为空
		if(null==member.getUpdated()){
			throw new ValidationException("校验失败，updated不能为空！");
		}
	}
	
	@SecDelete
	@RequestMapping("/delete/{id}")
	public @ResponseBody XjjJson delete(@PathVariable("id") Long id){
		memberService.delete(id);
		return XjjJson.success("成功删除1条");
	}
	@SecDelete
	@RequestMapping("/delete")
	public @ResponseBody XjjJson delete(@RequestParam("ids") Long[] ids){
		if(ids == null || ids.length == 0){
			return XjjJson.error("没有选择删除记录");
		}
		for(Long id : ids){
			memberService.delete(id);
		}
		return XjjJson.success("成功删除"+ids.length+"条");
	}
}

